/*package lab3;

import java.util.Scanner;
import java.util.ArrayList;

public class Player {

    boolean cdIn;
    
    public void player() {
    
  
    public void playCD() {
        System.out.println("Welcome using CDPlayer.");
        System.out.println("Choose what you want to do.");

        System.out.println("1: Put CD in");
        System.out.println("2: Play CD");
        System.out.println("3: Next Song");
        System.out.println("4: Previous Song");
        System.out.println("5: Show your CD tracks");
        System.out.println("6: Quit");
        
        while (true) {
            Scanner reader = new Scanner(System.in);
            String command = reader.nextLine();

            Cd cd1 = new Cd();
            
            if (command.equals("6")) {
                break;
            }

            if (command.equals("1")) {
                System.out.println("CD is in. It is ready to play.");
            }

            if (command.equals("2")) {
                System.out.println("Playing: " + cd1.track.get(0) + cd1.artist.get(0) + cd1.length.get(0));
            }

            if (command.equals("3")) {
                System.out.println("Playing: " + cd1.track.get(+1) + cd1.artist.get(+1) + cd1.length.get(+1));
            }
            if (command.equals("4")) {
                System.out.println("Playing: " + cd1.track.get(-1) + cd1.artist.get(-1) + cd1.length.get(-1));
            }
            if (command.equals("5")) {
                System.out.println("This CD contains these songs: ");

                System.out.println("Track 1 name: " + cd1.track.get(0) + " Artist name: " + cd1.artist.get(0) + " Length of the song: " + cd1.length.get(0));
                System.out.println("Track 2 name: " + cd1.track.get(1) + " Artist name: " + cd1.artist.get(1) + " Length of the song: " + cd1.length.get(1));
                System.out.println("Track 3 name: " + cd1.track.get(2) + " Artist name: " + cd1.artist.get(2) + " Length of the song: " + cd1.length.get(2));
                System.out.println("Track 4 name: " + cd1.track.get(3) + " Artist name: " + cd1.artist.get(3) + " Length of the song: " + cd1.length.get(3));
                System.out.println("Track 5 name: " + cd1.track.get(4) + " Artist name: " + cd1.artist.get(4) + " Length of the song: " + cd1.length.get(4));
            }
        }
    }
}
