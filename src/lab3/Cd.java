package lab3;

import java.util.ArrayList;
import java.util.Scanner;

public class Cd {

    ArrayList<String> track = new ArrayList<String>();
    ArrayList<String> artist = new ArrayList<String>();
    ArrayList<String> length = new ArrayList<String>();

    boolean cdIn = false;
    int number = 0;
    int tracknumber = 0;

    public void addTracks() {
        System.out.println("Burning tracks in CD...");

        track.add("Best House Remix Vol.1");
        artist.add("DJ Java");
        length.add("5:49");
        System.out.println("Track 1 added.");
        track.add("Rap Mix Vol.1");
        artist.add("MC Python");
        length.add("3:41");
        System.out.println("Track 2 added.");
        track.add("Best Rock Songs");
        artist.add("The CPlusPlus");
        length.add("8:10");
        System.out.println("Track 3 added.");
        track.add("Top 100 Pop Songs");
        artist.add("Ruby");
        length.add("12:30");
        System.out.println("Track 4 added.");
        track.add("Top 10 Techno Songs");
        artist.add("DJ Netbeans");
        length.add("7:21");
        System.out.println("Track 5 added.");

    }

    public void showTracks() {
        System.out.println("This CD contains these songs: ");

        System.out.println("Track 1 name: " + track.get(tracknumber) + " Artist name: " + artist.get(tracknumber) + " Length of the song: " + length.get(tracknumber));
        tracknumber = tracknumber + 1;
        System.out.println("Track 2 name: " + track.get(tracknumber) + " Artist name: " + artist.get(tracknumber) + " Length of the song: " + length.get(tracknumber));
        tracknumber = tracknumber + 1;

        System.out.println("Track 3 name: " + track.get(tracknumber) + " Artist name: " + artist.get(tracknumber) + " Length of the song: " + length.get(tracknumber));
        tracknumber = tracknumber + 1;

        System.out.println("Track 4 name: " + track.get(tracknumber) + " Artist name: " + artist.get(tracknumber) + " Length of the song: " + length.get(tracknumber));
        tracknumber = tracknumber + 1;

        System.out.println("Track 5 name: " + track.get(tracknumber) + " Artist name: " + artist.get(tracknumber) + " Length of the song: " + length.get(tracknumber));
        tracknumber = 0;

    }

    public void start() {
        System.out.println("Welcome using CDPlayer.");
        System.out.println("Choose what you want to do.");

        System.out.println("1: Put CD in");
        System.out.println("2: Play CD");
        System.out.println("3: Next Song");
        System.out.println("4: Previous Song");
        System.out.println("5: Show your CD tracks");
        System.out.println("6: Quit");

        while (true) {
            Scanner reader = new Scanner(System.in);
            String command = reader.nextLine();

            if (command.equals("6")) {
                break;
            }

            if (command.equals("1")) {
                System.out.println("CD is in. It is ready to play.");
            }

            if (command.equals("2")) {
                System.out.println("Playing: " + track.get(number) + ("-") + artist.get(number) + ("-") + length.get(number));
            }

            if (command.equals("3")) {
                number = number + 1;
                System.out.println("Playing: " + track.get(number) + ("-") + artist.get(number) + ("-") + length.get(number));
                if (number > 4) {
                    System.out.println("No more tracks");
                    continue;
                }
            }
            if (command.equals("4")) {
                number = number - 1;
                System.out.println("Playing: " + track.get(number) + ("-") + artist.get(number) + ("-") + length.get(number));
            }
            if (command.equals("5")) {
                showTracks();
                continue;
            }

        }

    }
}
