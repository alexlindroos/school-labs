/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author Alex
 */
public class CoffeeMaker {
    private int waterLevel;
    private int coffeeBeans;
    private boolean powerOn;
    private int counter;
    

public void coffeeMaker() {
    this.waterLevel = 1000;
    this.powerOn = true;
    this.coffeeBeans = 500;
    this.counter = 0;
    }

public void pressOff(){
    if(this.powerOn = true) {
        this.powerOn = false;
        this.waterLevel += -25;
        System.out.println("Rinsing the coffee maker...");
        System.out.println("Power is off!");
    }
}
public void pressOn(){
    if(this.powerOn = true) {
        this.powerOn = true;
        System.out.println("Power is on!");
    }
} 
public void pressCoffeeButton(){
    if(this.powerOn = true && this.waterLevel > 1 && this.coffeeBeans > 1){
        this.waterLevel += -150;
        this.coffeeBeans += -10;
        counter ++;
        System.out.println("Making coffee.");
        System.out.println("Coffee is ready!");
    }else{
        System.out.println("Error occured.");
         }
    
    if (this.waterLevel < 300){
        fillWater();
    }
    if (counter < 10){
        System.out.println("No need for cleaning.");
    }else{
            this.waterLevel += -300;
            System.out.println("Cleaning the machine...");
    }
        if (this.coffeeBeans <= 450){
            fillCoffee();
        }
     
}
public void pressEspresso(){
    if(this.powerOn = true && this.waterLevel >= 1 && this.coffeeBeans >= 1){
        this.waterLevel += -30;
        this.coffeeBeans += -8;
        counter ++;
        System.out.println("Making Espresso.");
        System.out.println("Espresso is ready!");
    }else{
        System.out.println("Error occured.");
         }
    
    if (this.waterLevel <= 300){
        fillWater();
    }
    if (counter < 10){
        System.out.println("No need for cleaning.");
    }else{
            this.waterLevel += -300;
            System.out.println("Cleaning the machine...");
        
    }
    
    if (this.coffeeBeans <= 450){
        fillCoffee();
    }

    
}

public void fillWater(){
    if(this.waterLevel < 300){
        this.waterLevel = 1000;
        System.out.println("Water tank is filled!");
    }
}
public void fillCoffee(){
    if(this.coffeeBeans < 450){
        this.coffeeBeans = 500;
        System.out.println("Coffee beans tank is filled!");
    }
}
public void status() {
    int water = waterLevel;
    int coffee = coffeeBeans;
    System.out.println("You have "+water+" ml of water left.");
    System.out.println("You have "+coffee+" grams of coffee left.");
    }
}