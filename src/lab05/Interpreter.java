package lab05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;

public class Interpreter implements Runnable, Observer {

    private PrintWriter output; // For what comes out of your computer
    private BufferedReader input; //For what comes to your computer
    private Socket connection;
    private String message;

    public Interpreter(Socket connection) {
        this.connection = connection;
        this.message = message;
    }

    public void run() {
        while (true) {
            try {
                setupStreams(); // Setting input and output stream
                whileChatting(); // When computers are connected you can send messages
            } catch (IOException ioException) {
                ioException.printStackTrace(); // So we see what failed
            }
        }
    }

    // get stream to send and receive data
    private void setupStreams() throws IOException {
        output = new PrintWriter(connection.getOutputStream()); // Creating the pathway for outputs
        output.flush(); // Cleaning 
        input = new BufferedReader(new InputStreamReader(connection.getInputStream())); // Creating the pathway for inputs
        System.out.println("Streams are now setup!");
    }

    private void whileChatting() throws IOException {
        output.println("SERVER - You are now connected!");
        do {
            message = (String) input.readLine();
            ChatHistory ch = ChatHistory.getInstance();
            ch.addMessage(message);
            System.out.println("\n" + message); // Shows the message
        } while (!message.equals("END"));
        closeChat();
    
    }

    private void closeChat() {
        output.println("\n Closing connections... \n");
        try {
            output.close(); // Closing output stream
            input.close(); // Closing input stream
            connection.close(); // Closing connection
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    
    public void newMessage(String message) {
        output.println(connection.getInetAddress()+": "+ message);
        output.flush(); // Cleans extra bytes
    }

}
