package lab05;

import java.util.ArrayList;

public class ChatHistory {

    private static final ChatHistory Instance = new ChatHistory();

    ArrayList<String> history = new ArrayList<String>();
    ArrayList<Observer> observers = new ArrayList<Observer>();

    private ChatHistory() {
    }

    public static ChatHistory getInstance() {
        return Instance;
    }

    public void addMessage(String message) {
        history.add(message);
        for (Observer ob : observers) {
            ob.newMessage(message);
        }
    }

    public String showHistory() {
        return history.get(history.size() - 1);

    }

    public void addObserver(Observer ob) {
        observers.add(ob);
    }

}
