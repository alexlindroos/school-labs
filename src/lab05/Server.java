package lab05;

import java.io.*;
import java.net.*;

public class Server {

    
    private ServerSocket server; // For the connection
    private Socket connection; // Connection between computers
    private String message; // Stores the message

    // Constructor
    public Server() {

    }

    public void startRunning() {
        try {
            server = new ServerSocket(6789, 50); // (portnumber and backlog)
            while (true) {
                try {
                    waitForConnection(); // Start and wait someone to connect
                    Interpreter interpreter = new Interpreter(connection);// Create new interpreter instance
                    ChatHistory ch = ChatHistory.getInstance();
                    Thread thread = new Thread(interpreter);//create new thread
                    ch.addObserver(interpreter);
                    thread.start();
                    
                    
                   
                } catch (EOFException eofException) {
                    System.out.println("\n Server ended the connection!");
                }
            }
        } catch (IOException ioException) {
            ioException.printStackTrace(); // So we see what failed
        }
    }

    // Wait for connection, then display connection information
    private void waitForConnection() throws IOException {
        System.out.println(" Waiting for someone to connect...\n");
        connection = server.accept(); //Accept connection to socket
        System.out.println(" Now connected to" + connection.getInetAddress().getHostName());
    }

}